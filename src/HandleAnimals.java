import java.util.ArrayList;

public class HandleAnimals {
    // Create a list of Animals so we know what code to execute.
    public static ArrayList<Animal> listAnimals = new ArrayList<Animal>();
    // Create a constructor to instantiate our animals.
    public HandleAnimals() {
        // Add each animal to the list.
        listAnimals.add(new Dog());
        listAnimals.add(new Cat());
    }
    // Create a method that will be used to run code in our main method.
    public static void doTrick() {
        // Loop through the contents of our listAnimals list.
        for (int i = 0; i < listAnimals.size(); i++) {
            // For each animal in the list we will execute the code contained in doTrick()
            listAnimals.get(i).doTrick();
        }
    }
}
