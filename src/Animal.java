// Create a special type of class called an abstract class that can be extended but not instantiated.
public abstract class Animal {
    // Create an abstract method that returns the string Name.
    public abstract String name();
    // Create an abstract method that executes code.
    public abstract void doTrick();
}
