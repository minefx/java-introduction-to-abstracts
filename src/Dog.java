// Create the class Dog that extends Animal
public class Dog extends Animal {
    // Implement methods.
    @Override
    public String name() {
        return "Albert";
    }

    // Write the code that you want to be executed when the animal does a trick.
    @Override
    public void doTrick() {
        System.out.println("Albert Rolls Over!");
    }
}
