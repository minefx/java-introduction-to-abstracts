public class Cat extends Animal {
    // Implement methods
    @Override
    public String name() {
        return "Felix the Cat";
    }

    // Write the code that you want to be executed when the animal does a trick.
    @Override
    public void doTrick() {
        System.out.println("Felix meows!");
    }
}
