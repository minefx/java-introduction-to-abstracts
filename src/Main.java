public class Main {
    // Create out main method.
    public static void main(String[] args) {
        // Define our doAnimals object we created.
        HandleAnimals doAnimals = new HandleAnimals();
        // Execute the code contained in the doTrick() method.
        HandleAnimals.doTrick();
    }
}
